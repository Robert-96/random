import random


def setUpRun():
    print("setUpRun")

def tearDownRun():
    print("tearDownRun")

def random_error():
	r = random.randint(1, 100)
	if r is 100:
		raise Exception("Random exception")


class Model_A:

    def setUpModel(self):
        print("Model_A.setUpModel")

    def tearDownModel(self):
        print("Model_A.tearDownModel")

    def vertex_0(self):
        print("Model_A.vertex_0")
        random_error()

    def vertex_1(self):
        print("Model_A.vertex_1")
        random_error()

    def vertex_2(self):
        print("Model_A.vertex_2")
        random_error()

    def vertex_3(self):
        print("Model_A.vertex_3")
        random_error()

    def vertex_4(self):
        print("Model_A.vertex_4")
        random_error()

    def vertex_5(self):
        print("Model_A.vertex_5")
        random_error()

    def vertex_6(self):
        print("Model_A.vertex_6")
        random_error()

    def vertex_7(self):
        print("Model_A.vertex_7")
        random_error()

    def edge_0(self):
        print("Model_A.edge_0")

    def edge_1(self):
        print("Model_A.edge_1")

    def edge_2(self):
        print("Model_A.edge_2")

    def edge_3(self):
        print("Model_A.edge_3")

    def edge_4(self):
        print("Model_A.edge_4")

    def edge_5(self):
        print("Model_A.edge_5")

    def edge_6(self):
        print("Model_A.edge_6")

    def edge_7(self):
        print("Model_A.edge_7")

    def edge_8(self):
        print("Model_A.edge_8")

    def edge_9(self):
        print("Model_A.edge_9")

    def edge_10(self):
        print("Model_A.edge_10")

    def edge_11(self):
        print("Model_A.edge_11")

    def edge_12(self):
        print("Model_A.edge_12")

    def edge_13(self):
        print("Model_A.edge_13")


class Model_B:

    def setUpModel(self):
        print("Model_B.setUpModel")

    def tearDownModel(self):
        print("Model_B.tearDownModel")

    def vertex_0(self):
        print("Model_B.vertex_0")
        random_error()

    def vertex_1(self):
        print("Model_B.vertex_1")
        random_error()

    def edge_0(self):
        print("Model_B.edge_0")

    def edge_1(self):
        print("Model_B.edge_1")
